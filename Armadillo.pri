message("Armadillo 9.200.6 in " $$PWD)

#include($$top_srcdir_libraries/Lapack/Lapack.pri)

DEFINES += ARMA_USE_BLAS
include($$top_srcdir_libraries/OpenBlas/OpenBlas.pri)

INCLUDEPATH += $$PWD/inc/

DISTFILES += \
    $$PWD/README.md \
    $$PWD/NOTICE.txt
